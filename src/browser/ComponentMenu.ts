import { ActionClass, ColumnClass, ComponentClass, Parser, StoreClass, StoreFieldClass } from "./Parser";
import { TypeMap } from "./TypeMap";

export class ComponentMenu extends Ext.menu.Menu {/* refs */
    public aNewAction: Ext.Action;
    public aAddAction: Ext.Action;
    public aAddStore: Ext.Action;
    public aAddStoreField: Ext.Action;
    public aAddComponent: Ext.Action;
    public aDelAction: Ext.Action;
    public aDelStore: Ext.Action;
    public aSetIdField: Ext.Action;
    public aDelStoreField: Ext.Action;
    public aDelComponent: Ext.Action;
    public aLayoutMenu: Ext.Action;
    public aXtypeMenu: Ext.Action;
    public aSetXtypeMenu: Ext.Action;
    public aPanelTBars: Ext.Action;
    public aGridFromStore: Ext.Action;
    public aAddCol: Ext.Action;
    public aSetRegion: Ext.Action;
    /* refs */

    public selectedNode: Ext.tree.TreeNode;
    public menuXtypes: Ext.menu.Menu;
    public menuLayouts: Ext.menu.Menu;
    public menuPanelToolbars: Ext.menu.Menu;
    public actionsMenu: Ext.menu.Menu;
    public storesMenu: Ext.menu.Menu;
    public menuRegion: Ext.menu.Menu;

    constructor(private tree: Ext.tree.TreePanel, config?) {
        super(config);
        this.tree.getSelectionModel().on("selectionchange", this.onTreeSelectionChange.bind(this));
        this.tree.on("contextmenu", this.onTreeContextmenu.bind(this));
    }

    public on_aNewAction_handler() {
        const actionName = "aNewAction" + Ext.id();
        const node = this.selectedNode.appendChild({
            iconCls: "icon-font",
            isTarget: false,
            nodeType: "node",
            text: actionName,
        });
        node.attributes.config = new ActionClass(node);
        node.attributes.config.text = actionName;
    }

    public on_aDelAction_handler() {
        const node = this.selectedNode;
        // this.selectedNode.previousSibling.select();
        node.remove(true);
    }

    public on_aAddStore_handler() {
        const node = this.selectedNode.appendChild({
            iconCls: "icon-database",
            nodeType: "node",
        });
        const c = node.attributes.config = new StoreClass(node);
        Ext.apply(c, {
            storeId: "NewStore",
            xtype: "jsonstore",
        });
    }

    public on_aDelStore_handler() {
        const node = this.selectedNode;
        // node.previousSibling.select();
        node.remove(true);
    }

    public on_aAddStoreField_handler() {
        const node = this.selectedNode.appendChild({
            iconCls: "icon-database_table",
            nodeType: "node",
        });
        node.attributes.config = new StoreFieldClass(node);
        Ext.apply(node.attributes.config, {
            name: "NewField",
            type: "auto",
        });
    }

    public on_aDelStoreField_handler() {
        this.selectedNode.remove(true);
    }

    public on_aDelComponent_handler() {
        const node = this.selectedNode;
        // node.previousSibling.select();
        node.remove(true);
    }

    public on_aAddCol_handler() {
        const config = {
            dataIndex: "field1",
            header: "field1",
        };
        const n = this.selectedNode.appendChild({
            iconCls: "icon-shape_align_top",
            nodeType: "node",
        });
        n.attributes.config = new ColumnClass(n);
        Ext.apply(n.attributes.config, config);

    }

    public initActions() {
        const actions = /* actDef */[{
            actionId: "aNewAction",
            hidden: true,
            iconCls: "icon-font_add",
            text: "New Action",
        }, {
            actionId: "aAddAction",
            hidden: true,
            iconCls: "icon-font_add",
            menu: this.actionsMenu,
            text: "Add Action",
        }, {
            actionId: "aAddStore",
            hidden: true,
            iconCls: "icon-database_add",
            text: "Add store",
        }, {
            actionId: "aAddStoreField",
            hidden: true,
            iconCls: "icon-textfield_add",
            text: "Add field",
        }, {
            actionId: "aAddComponent",
            hidden: true,
            iconCls: "icon-application_add",
            text: "Add component",
        }, {
            actionId: "aDelAction",
            hidden: true,
            iconCls: "icon-font_delete",
            text: "Del Action",
        }, {
            actionId: "aDelStore",
            hidden: true,
            iconCls: "icon-database_delete",
            text: "Del store",
        }, {
            actionId: "aSetIdField",
            iconCls: "icon-bullet_key",
            text: "Set as ID",
        }, {
            actionId: "aDelStoreField",
            hidden: true,
            iconCls: "icon-textfield_delete",
            text: "Del field",
        }, {
            actionId: "aDelComponent",
            hidden: true,
            iconCls: "icon-application_delete",
            text: "Del component",
        }, {
            actionId: "aLayoutMenu",
            hideOnClick: false,
            iconCls: "icon-font",
            menu: this.menuLayouts,
            text: "Set Layout",
        }, {
            actionId: "aXtypeMenu",
            hideOnClick: false,
            iconCls: "icon-application_add",
            menu: this.menuXtypes,
            text: "Add component",
        }, {
            actionId: "aSetXtypeMenu",
            hideOnClick: false,
            iconCls: "icon-application_add",
            text: "Set xtype",
        }, {
            actionId: "aPanelTBars",
            hideOnClick: false,
            iconCls: "icon-font",
            menu: this.menuPanelToolbars,
            text: "Add toolbar",
        }, {
            actionId: "aGridFromStore",
            hidden: true,
            hideOnClick: false,
            iconCls: "icon-application_view_detail",
            menu: this.storesMenu,
            text: "Grid from Store",
        }, {
            actionId: "aAddCol",
            hidden: true,
            iconCls: "icon-shape_align_top",
            text: "Add column",
        }, {
            actionId: "aSetRegion",
            hideOnClick: false,
            iconCls: "icon-font",
            menu: this.menuRegion,
            text: "Set region",
        }]/* actDef */;

        actions.forEach((action) => {
            this[action.actionId] = new Ext.Action(action);
        });
    }

    public initGui() {
        Ext.apply(this, {
            items: [
                this.aNewAction,
                this.aAddAction,
                this.aAddStore,
                this.aAddStoreField,
                // this.aAddComponent,
                this.aXtypeMenu,
                this.aGridFromStore,
                this.aAddCol,
                this.aDelAction,
                this.aDelStore,
                this.aDelStoreField,
                this.aSetIdField,
                this.aDelComponent,
                this.aLayoutMenu,
                this.aPanelTBars,
                this.aSetRegion,
                // this.aSetXtypeMenu
            ],
        });
    }

    public onTreeSelectionChange(sm: Ext.tree.DefaultSelectionModel, node: Ext.tree.TreeNode) {
        this.selectedNode = node;
    }

    public updateMenusState(node: Ext.tree.TreeNode) {
        const config = node.attributes.config || {};
        const isAction = !Ext.isEmpty(config.actionId);
        const canHaveMenu = Parser.isXType(config.xtype, "menuitem") || Parser.isXType(config.xtype, "button");
        const hasXtype = !Ext.isEmpty(config.xtype);
        const isFromComps = node.isAncestor(this.tree.getNodeById("comps"));
        const isContainer = [
            "panel", "container", "window", "compositefield",
            "tabpanel", "button", "menuitem", "buttongroup",
        ].indexOf(config.xtype) !== -1;
        const isGrid = ["grid", "treepanel", "listview"].indexOf(config.xtype) !== -1;
        const isToolbar = ["tbar", "bbar", "fbar", "buttons", "contextmenu"].indexOf(node.attributes.text) !== -1;
        const isColumn = node.attributes.config instanceof ColumnClass;

        this.aNewAction.setHidden(node.id !== "actions");
        this.aDelAction.setHidden(node.parentNode.id !== "actions");

        this.aAddStore.setHidden(node.id !== "stores");
        this.aDelStore.setHidden(node.parentNode.id !== "stores");

        this.aAddStoreField.setHidden(!Parser.isXType(config.xtype, "store"));
        this.aDelStoreField.setHidden(!Parser.isXType((node.parentNode.attributes.config || {}).xtype, "store"));
        this.aSetIdField.setHidden(!Parser.isXType((node.parentNode.attributes.config || {}).xtype, "store"));

        // this.aAddComponent.setHidden(!isFromComps && isGrid);
        const test = isFromComps && !isAction && (isToolbar || isContainer || canHaveMenu || isColumn) && !isGrid;
        this.aXtypeMenu.setHidden(!test);
        this.aDelComponent.setHidden(!(isFromComps || isColumn));
        this.aGridFromStore.setHidden(!isFromComps || isAction || !isContainer || isGrid);

        this.aSetRegion.setHidden((node.parentNode.attributes.config || {}).layout !== "border" || isToolbar);

        this.aAddCol.setHidden(!isGrid);

        // var test1 = isFromComps && !isAction && !isToolbar && (isContainer || !hasXtype);
        this.aLayoutMenu.setHidden(!isContainer || isGrid);

        const test2 = isFromComps && Parser.isXType(config.xtype, "panel") && !isAction;
        this.aPanelTBars.setHidden(!test2);

        this.aAddAction.setHidden(!(isToolbar || canHaveMenu));

        this.aSetXtypeMenu.setHidden(!(isFromComps && !isAction && !isToolbar));
    }

    public onTreeContextmenu(node: Ext.tree.TreeNode, e: Ext.EventObject) {
        if (!node.isSelected()) { node.select(); }
        this.updateMenusState(node);
        this.showAt(e.getXY());
        e.stopEvent();
    }

    public initXTypesMenu() {
        this.menuXtypes = new Ext.menu.Menu();
        const addComp = (item: any, e: Ext.EventObject) => {
            let config;
            // if (Parser.isGrid((this.selectedNode.attributes.config || {}).xtype)) {
            //     config = {
            //         dataIndex: "field1",
            //         header: "field1"
            //     }
            //     var n = this.selectedNode.appendChild({
            //         nodeType: "node",
            //         iconCls: "icon-shape_align_top"
            //     });
            //     n.attributes.config = new ColumnClass(n);

            // } else {
            config = {
                ref: Ext.id({}, "comp"),
                xtype: item.theXtype,
            };
            const n = this.selectedNode.appendChild({
                iconCls: Parser.genNodeIcon(config),
                nodeType: "node",
                text: Parser.genNodeText(config),
            });
            n.attributes.config = new ComponentClass(n);
            // }
            if (Parser.isXType(config.xtype, "field")) {
                config.fieldLabel = "Field Label";
            }
            Ext.apply(n.attributes.config, config);

        };
        this.menuXtypes.on("beforeshow", () => {
            const xtypes = Object.keys(Ext.ComponentMgr.types);
            let containers: any[] = ["container", "panel", "form", "tabpanel", "fieldset", "viewport", "window"];
            const fields = [];
            const grids = [];
            const trees = [];
            const menus = [];
            const others = [];
            const toolbars = [];

            xtypes.forEach((xtype) => {
                if (Parser.isXType(xtype, "field")) {
                    fields.push({
                        handler: addComp,
                        text: TypeMap[xtype] || xtype,
                        theXtype: xtype,
                        xtype: "menuitem",
                    });
                } else if (
                    Parser.isXType(xtype, "grid") ||
                    Parser.isXType(xtype, "listview") ||
                    Parser.isXType(xtype, "lvcolumn")) {
                    grids.push({
                        handler: addComp,
                        text: TypeMap[xtype] || xtype,
                        theXtype: xtype,
                        xtype: "menuitem",
                    });
                } else if (Parser.isXType(xtype, "treepanel")) {
                    trees.push({
                        handler: addComp,
                        text: TypeMap[xtype] || xtype,
                        theXtype: xtype,
                        xtype: "menuitem",
                    });
                } else if (Parser.isXType(xtype, "toolbar") || Parser.isXType(xtype, "tbitem")) {
                    toolbars.push({
                        handler: addComp,
                        text: TypeMap[xtype] || xtype,
                        theXtype: xtype,
                        xtype: "menuitem",
                    });
                } else if (xtype.indexOf("menu") !== -1) {
                    menus.push({
                        handler: addComp,
                        text: TypeMap[xtype] || xtype,
                        theXtype: xtype,
                        xtype: "menuitem",
                    });
                } else {
                    if (
                        containers.indexOf(xtype) === -1 &&
                        !Parser.isXType(xtype, "store") &&
                        !Parser.isXType(xtype, "chart") &&
                        !Parser.isXType(xtype, "menu") &&
                        !Parser.isXType(xtype, "menubaseitem")
                    ) {
                        others.push({
                            handler: addComp,
                            text: TypeMap[xtype] || xtype,
                            theXtype: xtype,
                            xtype: "menuitem",
                        });
                    }
                }
            });

            containers = containers.map((xtype) => {
                return {
                    handler: addComp,
                    text: TypeMap[xtype] || xtype,
                    theXtype: xtype,
                    xtype: "menuitem",
                };
            });

            this.menuXtypes.removeAll(true);
            this.menuXtypes.add([
                { xtype: "menuitem", hideOnClick: false, text: "Containers", menu: containers },
                { xtype: "menuitem", hideOnClick: false, text: "Fields", menu: fields },
                { xtype: "menuitem", hideOnClick: false, text: "Grids", menu: grids },
                { xtype: "menuitem", hideOnClick: false, text: "Trees", menu: trees },
                { xtype: "menuitem", hideOnClick: false, text: "Menus", menu: menus },
                { xtype: "menuitem", hideOnClick: false, text: "Toolbars", menu: toolbars },
                { xtype: "menuitem", hideOnClick: false, text: "Others", menu: others },
                { xtype: "menuitem", text: "Empty", handler: addComp, theXtype: "" },
            ] as Ext.menu.ItemConfig[]);
        });
    }

    public initLayoutMenu() {
        this.menuLayouts = new Ext.menu.Menu();
        const setLayOut = (item, e: Ext.EventObject) => {
            this.selectedNode.attributes.config.layout = item.text;
            this.fireEvent("layoutchanged", this.selectedNode);
            // var sm = this.tree.getSelectionModel();
            // sm.fireEvent("selectionchange", sm, this.selectedNode);
        };
        this.menuLayouts.on("beforeshow", () => {
            const layouts = Object.keys((Ext.Container as any).LAYOUTS);
            this.menuLayouts.removeAll(true);
            this.menuLayouts.add(layouts.map((layout) => {
                return {
                    handler: setLayOut,
                    text: layout,
                    xtype: "menuitem",
                };
            }));
        });
    }

    public initRegionMenu() {
        const setRegion = (item, e: Ext.EventObject) => {
            this.selectedNode.attributes.config.layout = item.text;
            if (item.text !== "center") {
                Ext.apply(this.selectedNode.attributes.config, {
                    split: true,
                    [/north|south/.test(item.text) ? "height" : "width"]: 300,
                });
            }
            this.fireEvent("layoutchanged", this.selectedNode);
        };
        this.menuRegion = new Ext.menu.Menu({
            items: [
                { text: "center", xtype: "menuitem", handler: setRegion },
                { text: "north", xtype: "menuitem", handler: setRegion },
                { text: "south", xtype: "menuitem", handler: setRegion },
                { text: "west", xtype: "menuitem", handler: setRegion },
                { text: "east", xtype: "menuitem", handler: setRegion },
            ],
        });

    }

    public initMenuPanelToolbars() {
        this.menuPanelToolbars = new Ext.menu.Menu();
        const addToolBar = (item, e) => {
            const n = this.selectedNode.appendChild({
                leaf: false,
                nodeType: "node",
                text: item.text,
            });
        };
        this.menuPanelToolbars.on("beforeshow", () => {
            this.menuPanelToolbars.removeAll(true);
            const tbs = ["tbar", "bbar", "fbar", "buttons", "contextmenu"].filter((p) => {
                return this.selectedNode.findChild("text", p) == null;
            });
            this.menuPanelToolbars.add(tbs.map((item) => {
                return {
                    handler: addToolBar,
                    text: item,
                    xtype: "menuitem",
                };
            }));
        });
    }

    public initAddActionMenu() {
        const addAction = (item, e) => {
            const config = {
                actionId: item.text,
            };
            const n = this.selectedNode.appendChild({
                iconCls: Parser.genNodeIcon(config),
                nodeType: "node",
                text: Parser.genNodeText(config),
            });
            n.attributes.config = new ComponentClass(n);
            Ext.apply(n.attributes.config, config);
        };
        this.actionsMenu = new Ext.menu.Menu();
        this.actionsMenu.on("beforeshow", () => {
            const tree: Ext.tree.TreePanel = this.selectedNode.getOwnerTree() as any;
            const actionNode = tree.getNodeById("actions");
            this.actionsMenu.removeAll(true);
            this.actionsMenu.add(actionNode.childNodes.map((n) => {
                return {
                    handler: addAction,
                    iconCls: n.attributes.iconCls,
                    text: n.attributes.config.actionId,
                    xtype: "menuitem",
                };
            }));
        });
    }

    public initStoresMenu() {
        const makeGrid = (menuitem, e) => {
            const n = this.selectedNode.appendChild({
                nodeType: "node",
            });
            n.attributes.config = new ComponentClass(n);
            Ext.apply(n.attributes.config, {
                columns: menuitem.node.attributes.config.fields.map((fld) => {
                    return {
                        dataIndex: fld.name,
                        header: fld.name,
                    };
                }),
                ref: "newGrid",
                store: menuitem.node.attributes.config.storeId,
                xtype: "grid",
            });
        };
        this.storesMenu = new Ext.menu.Menu();
        this.storesMenu.on("beforeshow", () => {
            const tree: Ext.tree.TreePanel = this.selectedNode.getOwnerTree() as any;
            const storesNode = tree.getNodeById("stores");

            this.storesMenu.removeAll(true);
            this.storesMenu.add(storesNode.childNodes.map((item) => {
                return {
                    handler: makeGrid,
                    node: item,
                    text: item.text,
                    xtype: "menuitem",
                };
            }));
        });
    }

    /* methods */
    public on_aSetIdField_handler() {
        const thisConfig = this.selectedNode.attributes.config;
        const parentConfig = this.selectedNode.parentNode.attributes.config;

        parentConfig.idProperty = thisConfig.name;
    }

    public initComponent() {
        this.initXTypesMenu();
        this.initLayoutMenu();
        this.initMenuPanelToolbars();
        this.initAddActionMenu();
        this.initStoresMenu();
        this.initRegionMenu();

        this.initActions();
        this.initGui();
        super.initComponent();
        this.autoConnect();
    }

    public autoConnect() {
        Ext.iterate(Object.getPrototypeOf(this), (key, value, obj) => {
            if (typeof obj[key] === "function") {
                const def = /on_(.+)_(.+)/.exec(key);
                if (def) {
                    if (def[2].toLowerCase() === "handler") {
                        this[def[1]].setHandler(this[key], this);
                    } else if (this[def[1]]) {
                        if (this[def[1]] instanceof Ext.util.Observable && this[def[1]].getSelectionModel) {
                            const sm = this[def[1]].getSelectionModel();
                            this[def[1]].relayEvents(sm, Object.keys(sm.events));
                        }
                        this[def[1]].on(def[2], this[key], this);
                    }
                }
            }
        });
    }
}
