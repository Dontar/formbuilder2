import * as Ext from "ext";

export class PropertyStoreHack extends Ext.grid.PropertyStore {
    public store: Ext.data.Store<any>;
    private source: any;
    constructor(grid, source?) {
        super(grid, source);
        this.store = new Ext.data.GroupingStore({
            groupDir: "DESC",
            groupField: "order",
            recordType: Ext.grid.PropertyRecord as any,
            sortInfo: { field: "order", direction: "DESC" },
        });
        this.store.on("update", this.onUpdate, this);
        if (source) {
            this.setSource(source);
        }
    }
    public constructProps(xtype): any[] {
        // var xtype = (cfg.xtype)?cfg.xtype:(cfg.items)?"container":"box";
        const props = require("./tpl/props2.json");
        const xtypeDef = props.classes.find((item) => {
            return item.xtype === xtype;
        });
        if (xtypeDef) {
            const cfgs: any[] = [];
            const pth = (xtypeDef.path as string).split("/");
            (props.classes as any[]).filter((item) => {
                return pth.indexOf(item.xtype) !== -1;
            }).forEach((item) => {
                item.cfg.forEach((c) => {
                    cfgs.push({
                        class: item.cls,
                        name: c.name,
                        order: pth.indexOf(item.xtype),
                        type: c.type,
                    });
                });
            });
            return cfgs;
        }
        return [];
    }
    public setSource(o, xtype?) {
        this.source = o;
        this.store.removeAll();
        if (o) {
            if (!xtype) {
                xtype = (o.xtype) ? o.xtype : (o.items) ? "container" : "box";
            }
            const data = [];
            this.constructProps(xtype).forEach((p) => {
                if ([
                    "tbar", "bbar", "fbar", "items", "menu", "buttons",
                    "columns", "node", "fields", "_node",
                ].indexOf(p.name) === -1 &&
                    typeof o[p.name] !== "function") {
                    p.value = o[p.name];
                    if (p.value === undefined) {
                        p.value = Ext.ns(p.class).prototype[p.name];
                    }
                    if (!this.isEditableValue(p.value)) {
                        try {
                            p.value = JSON.stringify(p.value);
                        } catch (e) {
                            return;
                        }
                    }
                    data.push(new Ext.grid.PropertyRecord(p, p.name));
                }
            });
            this.store.loadRecords({ records: data }, {}, true);
        }
    }
}
