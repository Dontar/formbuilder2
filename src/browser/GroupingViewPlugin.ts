import * as Ext from "ext";

export class GroupingViewPlugin implements Ext.IExtPlugin {
    public init(grid: Ext.grid.GridPanel) {
        grid.getView = function() {
            if (!this.view) {
                this.view = new Ext.grid.GroupingView(this.viewConfig);
            }
            return this.view;
        };
    }
}

Ext.preg("GroupingViewPlugin", GroupingViewPlugin);
