
import * as Ext from "ext";
import "ext/dist/theme-blue";
import "ext/dist/ux-style";

import * as CM from "codemirror";

import * as ELC from "electron";
import * as nodeFS from "fs";
import * as mime from "mime";
import * as PATH from "path";

import { ComponentMenu } from "./ComponentMenu";
import * as FS from "./filesys";
import { ActionClass, ColumnClass, Parser, StoreClass, StoreFieldClass } from "./Parser";
import { PropEditorMenu } from "./PropEditorMenu";
import * as PropEditors from "./PropEditors";
import { SourceEditor } from "./SourceEditor";

import { getMainMenu } from "./MainMenu";

import * as CT from "./ComponentTree";
import { GroupingViewPlugin } from "./GroupingViewPlugin";
import { PropertyStoreHack } from "./PropertyStoreHack";

const remote = ELC.remote;
const Menu = remote.Menu;

interface IRange {
    from: CM.Position;
    to: CM.Position;
}

// var Range: typeof AceAjax.Range = ace.require("ace/range").Range;
// var Search: typeof AceAjax.Search = ace.require("ace/search").Search;
// var Document: typeof AceAjax.Document = ace.require("ace/document").Document;
// var EditSession: typeof AceAjax.EditSession = ace.require("ace/edit_session").EditSession;

// interface IUserModel {

// }

Ext.grid.PropertyRecord = Ext.data.Record.create([
    { name: "name", type: "string" },
    "value",
    { name: "type", type: "string" },
    { name: "class", type: "string" },
    { name: "order", type: "int" },
]);

Ext.grid.PropertyStore = PropertyStoreHack;

Ext.override(Ext.grid.PropertyGrid, {
    setSource(o, xtype) {
        this.propStore.setSource(o, xtype);
    },
});

Ext.override(Ext.grid.PropertyColumnModel, {
    getCellEditor(colIndex, rowIndex) {
        const p = this.store.getProperty(rowIndex);
        const n = p.data.name;
        const t: string = p.data.type;
        const val = p.data.value;
        if (this.grid.customEditors[n]) {
            return this.grid.customEditors[n];
        }
        if (Ext.isDate(val)) {
            return this.editors.date;
        } else if (typeof val === "number") {
            return this.editors.number;
        } else if (typeof val === "boolean") {
            return this.editors.boolean;
        } else {
            if (t.toLowerCase().indexOf("date") !== -1) {
                return this.editors.date;
            } else if (t.toLowerCase().indexOf("number") !== -1) {
                return this.editors.number;
            } else if (t.toLowerCase().indexOf("boolean") !== -1) {
                return this.editors.boolean;
            } else {
                return this.editors.string;
            }
        }
    },
});

Ext.override(Ext.grid.PropertyColumnModel, {
    renderProp(prop, meta, rec) {
        const cls = rec.data.class;
        const vl = rec.data.value;
        const dv = Ext.ns(cls).prototype[prop];
        if (vl !== dv) {
            return `<b><i>${prop}</i></b>`;
        }
        return prop;
    },
});

Ext.reg("SearchField", (Ext.ux.form as any).SearchField);

Ext.override((Ext.ux.form as any).SearchField, {
    onTrigger1Click() {
        if (this.hasSearch) {
            this.el.dom.value = "";
            this.store.clearFilter();
            this.triggers[0].hide();
            this.hasSearch = false;
        }
    },

    onTrigger2Click() {
        const v = this.getRawValue();
        if (v.length < 1) {
            this.onTrigger1Click();
            return;
        }
        this.store.filter(this.paramName, v, true);
        this.hasSearch = true;
        this.triggers[0].show();
    },
});

export class MainWindow extends Ext.Viewport {
    /* refs */
    public treeFiles: Ext.tree.TreePanel;
    public panelCenter: Ext.TabPanel;
    public panelDesign: Ext.Panel;
    public comp254: Ext.TabPanel;
    public treeStruct: Ext.tree.TreePanel;
    public treeCmpList: Ext.tree.TreePanel;
    public grdProps: Ext.grid.PropertyGrid;
    public edFilter: Ext.Component;
    /* refs */
    public edSource: SourceEditor;

    public storePropGrid: Ext.data.Store<any>;

    public parser: Parser = new Parser();

    public model: {
        [field: string]: any;
        assign?: (o: any) => void;
    };

    public currentFile: Ext.tree.TreeNode;
    public currentFileNode: Ext.tree.TreeNode;

    public fileEditor: Ext.tree.TreeEditor;
    public updatingSource: boolean;

    public compMenu: ComponentMenu;

    public refreshFromUserEdit: Ext.util.DelayedTask = new Ext.util.DelayedTask(() => {
        const data = this.edSource.editor.getValue();
        try {
            if (data.search(/\/\* (?:designer|actDef|stores) \*\//) > -1) {
                this.parser.parseSource(this.treeStruct.getRootNode(), data);
                this.grdProps.setSource(null);
                this.panelDesign.removeAll(true);
                this.panelDesign.update("");

                this.panelDesign.add(this.parser.getGui());
                if (!this.panelDesign.hidden) {
                    this.panelDesign.doLayout();
                }
            }
        } catch (e) {
            this.panelDesign.removeAll(true);
            this.panelDesign.update(`
                <h3>${(e as Error).message}</h3>
                <div>${((e as Error).stack || "").split("\n").join("<br/>")}</div>
            `);
        }
    });

    public saving: boolean = false;

    public fsWatcher: nodeFS.FSWatcher;

    public refreshFromFileSelect: Ext.util.DelayedTask = new Ext.util.DelayedTask(() => {
        const refreshDesign = (data: string) => {
            try {
                if (data.search(/\/\* (?:designer|actDef|stores) \*\//) > -1) {
                    this.parser.parseSource(this.treeStruct.getRootNode(), data);
                    this.grdProps.setSource(null);
                    this.panelDesign.add(this.parser.getGui());
                    if (!this.panelDesign.hidden) {
                        this.panelDesign.doLayout();
                    }
                } else {
                    this.parser.isParsing = true;
                    this.treeStruct.getRootNode().removeAll();
                    this.parser.isParsing = false;
                }
            } catch (e) {
                this.panelDesign.removeAll(true);
                this.panelDesign.update(`
                    <h3>${(e as Error).message}</h3>
                    <div>${(e as Error).stack.split("\n").join("<br/>")}</div>
                `);
            }
        };
        this.panelDesign.removeAll(true);
        this.panelDesign.update("");

        FS.readFile(this.currentFile.id, { encoding: "utf-8" }).then((data) => {
            const mode = mime.lookup(PATH.extname(this.currentFile.id).slice(1));
            this.edSource.setMode(mode);
            this.updatingSource = true;
            this.edSource.editor.setValue(data);
            this.edSource.editor.getDoc().clearHistory();
            this.edSource.editor.getDoc().markClean();
            refreshDesign(data);
            this.onFileEdit(null);
            this.updatingSource = false;

            if (this.fsWatcher) {
                this.fsWatcher.close();
            }
            this.fsWatcher = nodeFS.watch(this.currentFile.id, this.refreshFromFileWatch.bind(this));
        });
    });

    private prSelectionBox: Ext.Element;
    get selectionBox(): Ext.Element {
        if (!this.prSelectionBox) {
            this.prSelectionBox = Ext.get("selectionBox");
            this.prSelectionBox.setVisibilityMode(Ext.Element.DISPLAY);
        }
        return this.prSelectionBox;
    }

    private prMouseBox: Ext.Element;
    get mouseBox(): Ext.Element {
        if (!this.prMouseBox) {
            this.prMouseBox = Ext.get("mouseBox");
            this.prMouseBox.setVisibilityMode(Ext.Element.DISPLAY);
        }
        return this.prMouseBox;
    }
    /* methods */

    public on_panelCenter_tabchange(sender, tab) {
        if (tab !== this.panelDesign) {
            this.edSource.editor.refresh();
        }
    }

    public on_panelDesign_afterrender() {
        const selectNodeByCompEl = (el: HTMLElement) => {
            let compEl = el;
            let found = false;
            while (compEl !== document.body) {
                if (compEl.id.indexOf("ext-comp") !== -1) {
                    found = true;
                    break;
                }
                compEl = compEl.parentElement;
            }
            if (found) {
                const comp = Ext.getCmp<Ext.BoxComponent>(compEl.id);
                if (comp) {
                    if ((comp as any)._node) {
                        this.treeStruct.getNodeById((comp as any)._node).select();
                    }
                }
            }
        };

        this.mouseBox.show();
        setTimeout(() => {
            this.mouseBox.alignTo(this.panelDesign.el, "tl-tl");
            this.mouseBox.setSize(this.panelDesign.el.getSize());
            this.panelDesign.on("resize", () => {
                this.mouseBox.alignTo(this.panelDesign.el, "tl-tl");
                this.mouseBox.setSize(this.panelDesign.el.getSize());
            });

        }, 500);
        this.mouseBox.on("click", (e: Ext.EventObject, el: HTMLElement) => {
            this.mouseBox.hide();
            const htmlEl = document.elementFromPoint(e.getPageX(), e.getPageY());
            this.mouseBox.show();
            selectNodeByCompEl(htmlEl as HTMLElement);
            (htmlEl as HTMLElement).click();
        });
        this.mouseBox.on("contextmenu", (e: Ext.EventObject, el: HTMLElement) => {
            this.mouseBox.hide();
            const htmlEl = document.elementFromPoint(e.getPageX(), e.getPageY());
            this.mouseBox.show();
            e.preventDefault();
            selectNodeByCompEl(htmlEl as HTMLElement);
            setTimeout(() => {
                this.compMenu.updateMenusState(this.treeStruct.getSelectionModel().getSelectedNode());
                this.compMenu.showAt(e.getXY());
            }, 0);
        });

        // this.panelDesign.el.on("click", (e: Ext.EventObject, el: HTMLElement) => {
        //     e.preventDefault();
        //     selectNodeByCompEl(el);
        // });
        // this.panelDesign.el.on("contextmenu", (e: Ext.EventObject, el) => {
        // });
    }
    public on_panelDesign_deactivate() {
        this.selectionBox.hide();
        this.mouseBox.hide();
    }
    public on_panelDesign_activate() {
        const node = this.treeStruct.getSelectionModel().getSelectedNode();
        if (node) {
            this.selectionBox.show();
        }
        this.mouseBox.show();
    }

    public on_fileEditor_complete(sender: Ext.tree.TreeEditor, newVal, oldVal) {
        let newName: any = PATH.parse(sender.editNode.id);
        newName.base = newVal;
        newName = PATH.format(newName);
        FS.stat(sender.editNode.id).then(() => {
            return FS.rename(sender.editNode.id, newName);
        }, () => {
            if (this.treeFiles.getSelectionModel().getSelectedNode().isLeaf()) {
                return FS.writeFile(newName, "");
            } else {
                return FS.mkdir(newName);
            }
        }).then(() => {
            sender.editNode.setId(newName);
        });
    }

    // onFileEdit(e: AceAjax.EditorChangeEvent) {
    public onFileEdit(e) {
        setTimeout((updatingSource) => {
            if (!this.edSource.editor.getDoc().isClean()) {
                this.panelCenter.getComponent<Ext.Panel>(1).setTitle("*Source");
                if (!updatingSource) {
                    this.refreshFromUserEdit.delay(1500);
                }
            } else {
                this.panelCenter.getComponent<Ext.Panel>(1).setTitle("Source");
            }
        }, 0, this.updatingSource);
    }

    public on_treeFiles_beforeselect(sender, newNode, oldNode) {
        if (!this.edSource.editor.getDoc().isClean()) {
            Ext.Msg.confirm("Warning", "You have unsaved changes. Do you want to save them?", (btn) => {
                if (btn === "yes") {
                    FS.writeFile(oldNode.id, this.edSource.editor.getValue()).then(() => {
                        this.panelCenter.getComponent<Ext.Panel>(1).setTitle("Source");
                        this.edSource.editor.getDoc().markClean();
                        newNode.select();
                    }, (err) => {
                        Ext.MessageBox.setIcon(Ext.MessageBox.ERROR);
                        Ext.Msg.alert("Error", err.message);
                    });
                }
            });
            return false;
        }
    }

    public refreshFromFileWatch(e, filename: string) {
        if (!this.saving) {
            if (!this.edSource.editor.getDoc().isClean()) {
                Ext.Msg.confirm("Warning", "You have unsaved changes. Do you want to save them?", (btn) => {
                    if (btn === "yes") {
                        FS.writeFile(this.currentFile.id, this.edSource.editor.getValue()).then(() => {
                            this.panelCenter.getComponent<Ext.Panel>(1).setTitle("Source");
                            this.edSource.editor.getDoc().markClean();
                            this.refreshFromFileSelect.delay(10);
                        }, (err) => {
                            Ext.MessageBox.setIcon(Ext.MessageBox.ERROR);
                            Ext.Msg.alert("Error", err.message);
                        });
                    }
                });
            } else {
                this.refreshFromFileSelect.delay(10);
            }
        }
    }

    public on_treeFiles_selectionchange(sender, node: Ext.tree.TreeNode) {
        if (!node) { return; }
        this.currentFileNode = node;
        if (node.isLeaf()) {
            this.currentFile = node;
            this.selectionBox.hide();
            this.refreshFromFileSelect.delay(100);
        }
    }

    public on_treeStruct_selectionchange(sender, node: Ext.tree.TreeNode) {
        if (node) {

            setTimeout(() => {
                let xtype;
                const cfg = node.attributes.config;
                if (cfg instanceof ActionClass) { xtype = "Ext.Action"; }
                if (cfg instanceof StoreClass) { xtype = cfg.xtype || "store"; }
                if (cfg instanceof StoreFieldClass) { xtype = "Ext.data.Field"; }
                if (cfg instanceof ColumnClass) { xtype = cfg.xtype || "gridcolumn"; }

                (this.grdProps as any).setSource(node.attributes.config || {}, xtype);
            }, 100);
            // this.grdProps.setSource(this.constructPropObj(node.attributes.config || {}));

            if (node.attributes.compId && this.panelCenter.getActiveTab() === this.panelDesign) {
                try {
                    const cmp = Ext.getCmp<Ext.BoxComponent>(node.attributes.compId);
                    if (cmp.ownerCt && cmp.ownerCt instanceof Ext.TabPanel) {
                        (cmp.ownerCt as Ext.TabPanel).setActiveTab(cmp);
                    }
                    // this.selectionBox = !this.selectionBox?Ext.get("selectionBox"):this.selectionBox;
                    this.selectionBox.show();
                    if (cmp.el) {
                        this.selectionBox.dom.style.zIndex = cmp.el.dom.style.zIndex + 1;
                    }
                    this.selectionBox.setBox(cmp.getBox(), true);
                } catch (e) {
                    // console.error(e);
                }
            }
        }
    }

    public getSrcPartRange(part: "refs" | "actions" | "stores" | "designer" | "intf"): IRange {
        const doc = this.edSource.editor.getDoc();
        const range = this.search(new RegExp(Parser.regExps[part], "g"));
        if (range) {
            range.from.ch += Parser.marker[part].length;
            range.to.ch -= Parser.marker[part].length;
        }
        return range;
    }

    public updateSource() {
        this.updatingSource = true;
        try {
            const doc = this.edSource.editor.getDoc();
            let range;

            this.edSource.editor.operation(() => {

                range = this.getSrcPartRange("intf");
                if (range) {
                    doc.replaceRange(this.parser.getStoresInterface(), range.from, range.to);
                }
                range = this.getSrcPartRange("refs");
                if (range) {
                    doc.replaceRange(
                        this.parser.getRefsSource(PATH.parse(this.currentFile.id).ext),
                        range.from, range.to);
                }
                range = this.getSrcPartRange("actions");
                if (range) {
                    doc.replaceRange(this.parser.getActionsSource(), range.from, range.to);
                }
                range = this.getSrcPartRange("stores");
                if (range) {
                    doc.replaceRange(this.parser.getStoresSource(), range.from, range.to);
                }
                range = this.getSrcPartRange("designer");
                if (range) {
                    doc.replaceRange(this.parser.getCompSource(), range.from, range.to);
                }
            });

        } finally {
            this.updatingSource = false;
        }
    }

    public updateDesignPanel() {
        if (!this.parser.isParsing /*&& node.isAncestor(this.parser.componentsNode)*/) {
            (() => {
                this.panelDesign.removeAll(true);
                this.panelDesign.update("");
                try {
                    this.panelDesign.add(this.parser.getGui());
                    if (!this.panelDesign.hidden) {
                        this.panelDesign.doLayout();
                    }
                } catch (e) {
                    this.panelDesign.removeAll(true);
                    this.panelDesign.update(`
                        <h3>${(e as Error).message}</h3>
                        <div>${((e as Error).stack || "").split("\n").join("<br/>")}</div>
                    `);
                }
                this.updateSource();
            }).defer(300);
        }
    }

    public search(needle: string | RegExp): IRange {
        let regExp;
        if (typeof needle === "string") {
            regExp = new RegExp(Parser.esc(needle), "g");
        } else {
            regExp = needle;
        }
        const source = this.edSource.editor.getValue();
        const rgResult = regExp.exec(source);
        if (rgResult) {
            const doc = this.edSource.editor.getDoc();
            return { from: doc.posFromIndex(rgResult.index), to: doc.posFromIndex(regExp.lastIndex) };
        }
    }

    public addMethod(methodName: string, body?: string[]) {
        const doc = this.edSource.editor.getDoc();
        const range = this.search("/* methods */");
        if (range) {
            doc.setSelection(range.from, range.to);

            doc.replaceSelection([
                "",
                // `        ${methodName}() {`,
                `${methodName}() {`,
                "",
                // "        }",
                "}",
                "",
            ].join((doc as any).lineSeparator()), "end");
            (this.edSource.editor as any).indentSelection();
        }
    }

    public on_treeStruct_beforedblclick(node: Ext.tree.TreeNode, e: Ext.EventObject) {
        const cfg = (node.attributes.config || {});
        const doc = this.edSource.editor.getDoc();
        if (cfg.actionId) {
            const range = this.search(`on_${cfg.actionId}_handler`);
            if (range) {
                this.edSource.editor.scrollIntoView(range.from);
                doc.setSelection(range.from, range.to);
            } else {
                this.addMethod(`on_${cfg.actionId}_handler`);
            }
            return false;
        }
        if (cfg.storeId) {
            const range = this.search(new RegExp("storeId\\s*:\\s*" + `\"${cfg.storeId}\"`, "g"));
            if (range) {
                this.edSource.editor.scrollIntoView(range.from);
                doc.setSelection(range.from, range.to);
                return false;
            }
        }
        if (cfg.ref) {
            const range = this.search(new RegExp("ref\\s*:\\s*" + `\"${Parser.esc(cfg.ref)}\"`, "g"));
            if (range) {
                this.edSource.editor.scrollIntoView(range.from);
                doc.setSelection(range.from, range.to);
                return false;
            }
        }
    }

    public on_compMenu_layoutchanged(node) {
        this.grdProps.setSource(node.attributes.config);
        this.updateDesignPanel();
    }

    public on_treeStruct_append(tree, parent, node: Ext.tree.TreeNode, index) {
        this.updateDesignPanel();
    }

    public on_treeStruct_remove(tree, parent, node) {
        this.updateDesignPanel();
    }

    public on_treeStruct_movenode(tree, node, oldParent, newParent, index) {
        this.updateDesignPanel();
    }

    public on_grdProps_beforepropertychange(src: any, prop: string, newVal, oldVal) {
        if (typeof newVal === "string") {
            if (/^[\[\{]/.test(newVal.trim())) {
                try {
                    src[prop] = JSON.parse(newVal);
                    this.storePropGrid.getById(prop).commit();
                    return false;
                } catch (e) {
                    src[prop] = newVal;
                }
            }
        }
    }

    public on_storePropGrid_update(store, record, operation) {
        if (operation === Ext.data.Record.EDIT) {
            // var node = this.treeStruct.getSelectionModel().getSelectedNode();
            this.updateDesignPanel();
        }
    }

    public on_storePropGrid_remove(store, record, index) {
        const node = this.treeStruct.getSelectionModel().getSelectedNode();
        node.attributes.config[record.id] = undefined;
        this.updateDesignPanel();
    }

    public on_edSource_afterrender() {
        this.edSource.editor.on("change", this.onFileEdit.bind(this));
    }

    public on_panelDesign_resize(sender, adjWidth, adjHeight, rawWidth, rawHeight) {
        const s = this.treeStruct.getSelectionModel();
        this.on_treeStruct_selectionchange(s, s.getSelectedNode());
    }

    public getDir(path: string, cb: (result, e) => void, scope?: any) {
        scope = scope || this;
        FS.readdir(path).then((files) => {
            cb.call(scope, files.map((item) => {
                return {
                    iconCls: this.getFileTypeIcon(PATH.extname(item.path)),
                    id: item.path,
                    leaf: !item.stat.isDirectory(),
                    text: item.name,
                };
            }), { status: true });
        }, (err) => {
            cb.call(scope, null, err);
        });
    }

    public initComponent() {

        Ext.form.DateField.prototype.altFormats += "|c";
        this.initActions();
        this.initStores();
        this.initGui();
        super.initComponent();

        this.initModel({ /* bind */ /* bind */ });

        this.storePropGrid = this.grdProps.getStore();
        (this.edFilter as any).store = this.storePropGrid;

        this.fileEditor = new Ext.tree.TreeEditor(this.treeFiles, {
            xtype: "textfield",
        });
        this.compMenu = new ComponentMenu(this.treeStruct);

        this.treeFiles.getRootNode().setId((remote.process.argv.length > 1) ? remote.process.argv[1] : ".");
        const loader = this.treeFiles.getLoader();
        loader.directFn = this.getDir.bind(this);
        loader.baseAttrs = {
            singleClickExpand: true,
        };
        this.treeFiles.getRootNode().expand();

        const treeSorter = new Ext.tree.TreeSorter(this.treeFiles, {
            folderSort: true,
        });

        this.edSource = new SourceEditor({
            gutters: ["CodeMirror-linenumbers"],
            indentUnit: 4,
            lineNumbers: true,
        } as CM.EditorConfiguration);
        (this.panelCenter.items.item(1) as Ext.Container).add(this.edSource);
        (this.grdProps as any).customEditors = PropEditors.getPropEditors(this.treeStruct, this.grdProps);
        const propGridMenu = new PropEditorMenu(this.grdProps);
        this.autoConnect();

        const pcm: Ext.grid.PropertyColumnModel = this.grdProps.getColumnModel() as Ext.grid.PropertyColumnModel;
        pcm.setConfig([
            { header: pcm.nameText, width: 50, sortable: true, dataIndex: "name", id: "name", menuDisabled: true },
            { header: pcm.valueText, width: 50, resizable: false, dataIndex: "value", id: "value", menuDisabled: true },
            { header: "Class", dataIndex: "order", hidden: true, groupRenderer: (v, m, r) => r.data.class },
        ]);

        this.treeCmpList.setRootNode(CT.getRootCfg());

    }

    public initActions() {
        const actions = /* actDef */[]/* actDef */;
        actions.forEach((action) => {
            this[action.actionId] = new Ext.Action(action);
        });
    }

    public initStores() {
        const stores = /* stores */[]/* stores */;
        stores.forEach((store) => {
            const storeId = store.storeId;
            delete store.storeId;
            this[storeId] = Ext.create(store);
        });
    }

    public initGui() {
        Ext.apply(this, this.processGui(/* designer */{
            items: [{
                animate: true,
                autoScroll: true,
                ref: "treeFiles",
                region: "west",
                root: {
                    id: ".",
                    nodeType: "async",
                    text: "Root",
                },
                rootVisible: false,
                split: true,
                title: "Files",
                useArrows: true,
                width: 300,
                xtype: "treepanel",
            }, {
                activeTab: 0,
                deferredRender: false,
                items: [{
                    bodyBorder: false,
                    bodyStyle: "padding: 5px;background-color: #dfe8f6",
                    layout: "fit",
                    ref: "../panelDesign",
                    title: "Design",
                    xtype: "panel",
                }, {
                    layout: "fit",
                    title: "Source",
                    xtype: "panel",
                }],
                layoutOnTabChange: true,
                ref: "panelCenter",
                region: "center",
                xtype: "tabpanel",
            }, {
                items: [{
                    activeTab: 0,
                    height: 400,
                    items: [{
                        animate: true,
                        autoScroll: true,
                        enableDD: true,
                        ref: "../../treeStruct",
                        root: {
                            draggable: false,
                            id: "root",
                            nodeType: "node",
                            text: "Root",
                        },
                        rootVisible: false,
                        title: "Structure",
                        useArrows: true,
                        xtype: "treepanel",
                    }, {
                        autoScroll: true,
                        ref: "../../treeCmpList",
                        title: "Components",
                        xtype: "treepanel",
                    }],
                    ref: "../comp254",
                    region: "north",
                    split: true,
                    xtype: "tabpanel",
                }, {
                    plugins: ["GroupingViewPlugin"],
                    ref: "../grdProps",
                    region: "center",
                    tbar: [{
                        xtype: "tbfill",
                    }, {
                        fieldLabel: "Field Label",
                        paramName: "name",
                        ref: "../../../edFilter",
                        store: "storePropGrid",
                        xtype: "SearchField",
                    }],
                    xtype: "propertygrid",
                }],
                layout: "border",
                region: "east",
                split: true,
                width: 300,
                xtype: "container",
            }],
            layout: "border",
        }/* designer */));
    }

    public processGui(config: any) {
        if (typeof config.actionId === "string") {
            return this[config.actionId];
        }

        if (typeof config.store === "string") {
            config.store = this[config.store];
        }

        ["tbar", "bbar", "fbar", "items", "menu", "buttons"].forEach((prop) => {
            if (config[prop] !== undefined) {
                config[prop] = config[prop].map(this.processGui.bind(this));
            }
        });

        return config;
    }

    public initModel(bind: { [field: string]: Ext.form.Field }) {
        const model: any = this.model || {};
        model.assign = function(o) {
            Ext.apply(this, o);
        };

        Ext.iterate(bind, (key: string, val: any) => {
            if (model[key]) {
                bind[key].setValue(model[key]);
            }
            Object.defineProperty(model, key, {
                enumerable: true,
                get: bind[key].getValue.bind(bind[key]),
                set: bind[key].setValue.bind(bind[key]),
            });
        });

        Object.defineProperty(this, "model", {
            enumerable: true,
            get: () => {
                return model;
            },
            set: (v) => {
                Ext.apply(model, v);
            },
        });
    }

    public autoConnect() {
        Ext.iterate(Object.getPrototypeOf(this), (key, value, obj) => {
            if (typeof obj[key] === "function") {
                const def = /on_(.+)_(.+)/.exec(key);
                if (def) {
                    if (def[2].toLowerCase() === "handler") {
                        this[def[1]].setHandler(this[key], this);
                    } else if (this[def[1]]) {
                        if (this[def[1]] instanceof Ext.util.Observable && this[def[1]].getSelectionModel) {
                            const sm = this[def[1]].getSelectionModel();
                            this[def[1]].relayEvents(sm, Object.keys(sm.events));
                        }
                        this[def[1]].on(def[2], this[key], this);
                    }
                }
            }
        });
    }

    private getFileTypeIcon(fileType: string) {
        const iconCls = "icon-" + fileType.substr(1);
        if (Ext.util.CSS.getRule("." + iconCls) != null) {
            return iconCls;
        }
        return undefined;
    }
}

Ext.onReady(() => {
    const appView = new MainWindow();
    Menu.setApplicationMenu(getMainMenu(appView));
});
