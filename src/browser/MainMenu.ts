import { app, remote } from "electron";
import * as PATH from "path";
import * as FILE from "./filesys";
import { MainWindow } from "./FormBuilder";

class FileCommands {
    public currentFileNode: Ext.tree.TreeNode;
    public currentFile: Ext.tree.TreeNode;
    constructor(private appView: MainWindow) { }
    public newFile(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "New file"),
            leaf: true,
            text: "New file",
        });
        node.expand();
        n.select();
        this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);

    }
    public newTSmain(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "main.ts"),
            leaf: true,
            text: "main.ts",
        });
        FILE.writeFile(n.id, require("./tpl/ts/main.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });

    }
    public newTScontainer(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "container.ts"),
            leaf: true,
            text: "container.ts",
        });
        FILE.writeFile(n.id, require("./tpl/ts/container.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });

    }
    public newTSpanel(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "panel.ts"),
            leaf: true,
            text: "panel.ts",
        });
        FILE.writeFile(n.id, require("./tpl/ts/panel.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });

    }
    public newTSwindow(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "window.ts"),
            leaf: true,
            text: "window.ts",
        });
        FILE.writeFile(n.id, require("./tpl/ts/window.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });

    }
    public newTSindex(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "index.html"),
            leaf: true,
            text: "index.html",
        });
        FILE.writeFile(n.id, require("./tpl/ts/index.html.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });

    }
    public newJS(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        const n = node.appendChild({
            id: PATH.join(node.id, "New_file.js"),
            leaf: true,
            text: "New_file.js",
        });
        FILE.writeFile(n.id, require("./tpl/js/JSTemplate.tpl")).then(() => {
            node.expand();
            n.select();
            this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
        });
    }
    public newFolder(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        FILE.stat(node.id).then((stats) => {
            if (stats.isDirectory()) {
                const n = node.appendChild({
                    id: PATH.join(node.id, "New folder"),
                    leaf: false,
                    text: "New folder",
                });
                node.expand();
                n.select();
                this.appView.fileEditor.triggerEdit.defer(100, this.appView.fileEditor, [n]);
            }
        });
    }
    public openFile(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const dialog = remote.dialog;
        dialog.showOpenDialog(win, { properties: ["openFile"] }, Ext.emptyFn as any);
    }
    public openFolder(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const dialog = remote.dialog;
        dialog.showOpenDialog(win, { properties: ["openDirectory"] }, (files) => {
            this.appView.treeFiles.getRootNode().id = files[0];
            (this.appView.treeFiles.getRootNode() as Ext.tree.AsyncTreeNode).reload();
        });
    }
    public renameFileFolder(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        if (node) {
            this.appView.fileEditor.triggerEdit(node);
        }
    }
    public deleteFileFolder(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const node = this.currentFileNode;
        FILE.stat(node.id).then(
            (stats) => stats.isDirectory() ? FILE.rmdir(node.id) : FILE.unlink(node.id),
        ).then(
            () => { node.remove(true); },
            (err) => {
                Ext.Msg.setIcon(Ext.MessageBox.ERROR);
                Ext.Msg.alert("Error", err.message);
            },
        );
    }
    public revertFile(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        this.appView.on_treeFiles_selectionchange(this.appView.treeFiles.getSelectionModel(), this.appView.currentFile);
    }
    // public closeFile(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) { }
    public saveFile(menuItem: Electron.MenuItem, win: Electron.BrowserWindow) {
        const selectedFile = this.currentFile;
        this.appView.saving = true;
        FILE.writeFile(selectedFile.id, this.appView.edSource.editor.getValue()).then(() => {
            this.appView.panelCenter.getComponent<Ext.Panel>(1).setTitle("Source");
            this.appView.edSource.editor.getDoc().clearHistory();
            this.appView.edSource.editor.getDoc().markClean();
            this.appView.saving = false;
        }, (err) => {
            Ext.MessageBox.setIcon(Ext.MessageBox.ERROR);
            Ext.Msg.alert("Error", err.message);
            this.appView.saving = false;
        });
    }
}

export function getMainMenu(appView: MainWindow) {
    const c = new FileCommands(appView);
    const fileMenu = remote.Menu.buildFromTemplate([
        {
            label: "New File",
            submenu: [
                {
                    click: c.newJS.bind(c),
                    label: "New JavaScript",
                }, {
                    label: "New TypeScript",
                    submenu: [
                        { label: "index", click: c.newTSindex.bind(c) },
                        { label: "main", click: c.newTSmain.bind(c) },
                        { label: "container", click: c.newTScontainer.bind(c) },
                        { label: "panel", click: c.newTSpanel.bind(c) },
                        { label: "window", click: c.newTSwindow.bind(c) },
                    ],
                    // click: c.newTS.bind(c)
                }, {
                    click: c.newFile.bind(c),
                    label: "Other...",
                },
            ],
        },
        {
            accelerator: "CmdOrCtrl+Shift+N",
            click: c.newFolder.bind(c),
            label: "New Folder",
        },
        {
            type: "separator",
        },
        // {
        //     label: "Open File...",
        //     accelerator: "Ctrl+O",
        //     click: c.openFile.bind(c)
        // },
        {
            accelerator: "Ctrl+Shift+O",
            click: c.openFolder.bind(c),
            label: "Open Folder...",
        },
        {
            type: "separator",
        },
        {
            accelerator: "Ctrl+S",
            click: c.saveFile.bind(c),
            label: "Save",
        },
        {
            type: "separator",
        },
        {
            click: c.renameFileFolder.bind(c),
            label: "Rename",
        },
        {
            click: c.revertFile.bind(c),
            label: "Revert File",
        },
        // {
        //     label: "Close File",
        //     click: c.closeFile.bind(c)
        // },
        {
            click: c.deleteFileFolder.bind(c),
            label: "Delete",
        },
        {
            type: "separator",
        },
        {
            label: "Exit",
            role: "close",
        },
    ]);
    const editMenu = remote.Menu.buildFromTemplate([
        {
            accelerator: "CmdOrCtrl+Z",
            label: "Undo",
            role: "undo",
        },
        {
            accelerator: "Shift+CmdOrCtrl+Z",
            label: "Redo",
            role: "redo",
        },
        {
            type: "separator",
        },
        {
            accelerator: "CmdOrCtrl+X",
            label: "Cut",
            role: "cut",
        },
        {
            accelerator: "CmdOrCtrl+C",
            label: "Copy",
            role: "copy",
        },
        {
            accelerator: "CmdOrCtrl+V",
            label: "Paste",
            role: "paste",
        },
        {
            accelerator: "CmdOrCtrl+A",
            label: "Select All",
            role: "selectall",
        },
    ]);
    const viewMenu = remote.Menu.buildFromTemplate([
        {
            accelerator: "CmdOrCtrl+R",
            label: "Reload",
            click(item, focusedWindow) {
                if (focusedWindow) {
                    focusedWindow.reload();
                }
            },
        },
        {
            accelerator: (() => {
                if (process.platform === "darwin") {
                    return "Ctrl+Command+F";
                } else {
                    return "F11";
                }
            })(),
            label: "Toggle Full Screen",
            click(item, focusedWindow) {
                if (focusedWindow) {
                    focusedWindow.setFullScreen(!focusedWindow.isFullScreen());
                }
            },
        },
        {
            accelerator: "F12",
            label: "Toggle Developer Tools",
            click(item, focusedWindow) {
                if (focusedWindow) {
                    focusedWindow.webContents.openDevTools();
                }
            },
        },
    ]);

    appView.treeFiles.getSelectionModel().on("selectionchange", (sender, node: Ext.tree.TreeNode) => {
        if (node) {
            c.currentFileNode = node;
            if (node.isLeaf()) { c.currentFile = node; }
            const m = fileMenu.items;
            (m[0] as any).enabled = !node.isLeaf();
            (m[1] as any).enabled = !node.isLeaf();
            (m[5] as any).enabled = node.isLeaf() && appView.edSource.editor.getDoc().isClean();

        }
    });

    appView.treeFiles.on("contextmenu", (node, e) => {
        node.select();
        const [x, y] = e.getXY();
        setTimeout(() => { fileMenu.popup(remote.getCurrentWindow(), { x, y }); });

        // e.stopEvent();
    });
    appView.treeFiles.on("containercontextmenu", (tree: Ext.tree.TreePanel, e) => {
        c.currentFileNode = tree.getRootNode();
        const [x, y] = e.getXY();
        (fileMenu.items[0] as any).enabled = true;
        (fileMenu.items[1] as any).enabled = true;
        setTimeout(() => { fileMenu.popup(remote.getCurrentWindow(), { x, y }); });
        // e.stopEvent();
    });

    if (!appView.edSource.rendered) {
        appView.edSource.on("render", (sender: Ext.form.Field) => {
            const el = sender.getEl();
            el.on("contextmenu", (e: Ext.EventObject) => {
                e.stopEvent();
                const [x, y] = e.getXY();
                editMenu.popup(remote.getCurrentWindow(), { x, y });
            });
        });
    } else {
        const el = appView.edSource.getEl();
        el.on("contextmenu", (e: Ext.EventObject) => {
            e.stopEvent();
            const [x, y] = e.getXY();
            editMenu.popup(remote.getCurrentWindow(), { x, y });
        });
    }

    const mainMenu = new remote.Menu();
    mainMenu.append(new remote.MenuItem({ label: "File", submenu: fileMenu }));
    mainMenu.append(new remote.MenuItem({ label: "Edit", submenu: editMenu }));
    mainMenu.append(new remote.MenuItem({ label: "View", submenu: viewMenu }));
    return mainMenu;
}
