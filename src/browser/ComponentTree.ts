// tslint:disable-next-line:no-var-requires
const data: {classes: any[]} = require("./tpl/props2.json");

class Components {
    public cls: string;
    public path: string;
    public xtype: string;

    public nodeType: string = "async";
    public depth: number;

    public expanded: boolean = true;

    constructor(config: any) {
        this.cls = config.cls;
        this.path = config.path;
        this.xtype = config.xtype;
        this.depth = this.path.split("/").length;
    }

    get id(): string {
        return this.xtype;
    }

    get text(): string {
        return this.cls;
    }

    get children(): any[] {
        return data.classes.filter((item) => {
            return item.path.indexOf(this.path) !== -1 && item.path.split("/").length === this.depth + 1;
        }).map((item) => {
            return new Components(item);
        });
    }
}

export function getRootCfg() {
    return new Components({
        cls: "Ext.util.Observable",
        path: "Ext.util.Observable",
        xtype: "Ext.util.Observable",
    });
}
