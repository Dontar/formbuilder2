import { TypeMap } from "./TypeMap";

class PropProxy extends Ext.data.DataProxy {
    public data: any[];
    public api = { read: true };

    constructor(config?) {
        super(config);
    }

    public doRequest(action: string, rs, params, reader: Ext.data.DataReader, callback: () => any, scope, options) {
        params = params || {};
        let result;
        try {
            let data = this.data.filter((item: any) => {
                if (Ext.isObject(item)) {
                    return !Ext.isEmpty(params.query) ? item.name.search(params.query) !== -1 : true;
                }
                return !Ext.isEmpty(params.query) ? item.search(params.query) !== -1 : true;
            });
            if (data.length > 0) {
                if (Ext.isString(data[0])) {
                    data = data.map((item) => {
                        return {
                            name: item,
                            value: item,
                        };
                    });
                }
            }

            result = reader.readRecords(data);
        } catch (e) {
            // @deprecated loadexception
            this.fireEvent("loadexception", this, null, options, e);

            this.fireEvent("exception", this, "response", action, options, null, e);
            callback.call(scope, null, options, false);
            return;
        }
        callback.call(scope, result, options, true);
    }

}

// tslint:disable-next-line:max-classes-per-file
class XtypesProxy extends PropProxy {
    public get data(): any[] {
        return Object.keys(Ext.ComponentMgr.types).concat(Object.keys((Ext.grid.Column as any).types)).map((xtype) => {
            return {
                // name: TypeMap[xtype] || xtype,
                name: xtype,
                value: xtype,
            };
        });
    }
}

// tslint:disable-next-line:max-classes-per-file
class LayoutsProxy extends PropProxy {
    public get data(): any[] {
        return Object.keys((Ext.Container as any).LAYOUTS);
    }
}
// tslint:disable-next-line:max-classes-per-file
class ActionsProxy extends PropProxy {
    constructor(private tree: Ext.tree.TreePanel) {
        super();
    }

    public get data(): any[] {
        const actionsNode = this.tree.getNodeById("actions");
        return actionsNode.childNodes.map((item) => {
            return item.text;
        });
    }
}

// tslint:disable-next-line:max-classes-per-file
class StoresProxy extends PropProxy {
    constructor(private tree: Ext.tree.TreePanel) {
        super();
    }

    public get data(): any[] {
        const storesNode = this.tree.getNodeById("stores");
        return storesNode.childNodes.map((item) => {
            return item.attributes.config.storeId;
        });
    }
}

// tslint:disable-next-line:max-classes-per-file
class StoreFieldsProxy extends PropProxy {
    public node: Ext.tree.TreeNode;
    constructor(private tree: Ext.tree.TreePanel) {
        super();
    }

    public get data() {
        this.node = this.tree.getSelectionModel().getSelectedNode();
        const sNode = this.tree.getNodeById("stores");
        let storeNode;

        if (this.node.isAncestor(sNode)) {
            storeNode = this.node;
        } else {
            try {
                const storeName = (this.node.parentNode.attributes.config || {}).store;
                if (storeName) {
                    storeNode = sNode.findChildBy((node) => {
                        return (node.attributes.config || {}).storeId === storeName;
                    });
                } else { return []; }
            } catch (e) {
                return [];
            }
        }
        return storeNode.childNodes.map((node) => {
            return node.attributes.config.name;
        });

        // return ["No fields"];
    }
}

// tslint:disable-next-line:max-classes-per-file
class CSSRulesProxy extends PropProxy {
    public get data(): any[] {
        return Object.keys(Ext.util.CSS.getRules()).filter((rule) => {
            return rule.indexOf(".icon-") === 0;
        }).map((rule) => {
            return rule.slice(1);
        });
    }
}

export function getPropEditors(tree: Ext.tree.TreePanel, propGrid: Ext.grid.PropertyGrid) {
    let isAction: boolean = true;
    // let selectedNode: Ext.tree.TreeNode;
    (tree as Ext.tree.TreePanel).getSelectionModel().on("selectionchange", (treeSel, node: Ext.tree.TreeNode) => {
        if (node) {
            const actionNode = tree.getNodeById("actions");
            isAction = !Ext.isEmpty(actionNode) && node.isAncestor(actionNode);
            // selectedNode = node;
        }
    });

    return {
        _xtype: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new XtypesProxy(),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        get xtype() {
            this._xtype.field.getStore().load();
            return this._xtype;
        },

        _layout: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new LayoutsProxy(),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        get layout() {
            this._layout.field.getStore().load();
            return this._layout;
        },

        region: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            editable: true,
            forceSelection: false,
            store: ["center", "north", "south", "east", "west"],
            triggerAction: "all",
            // valueField: "value",
            // displayField: "name",
        })),

        type: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            editable: true,
            forceSelection: false,
            store: ["auto", "string", "int", "float", "boolean", "date"],
            triggerAction: "all",
            // valueField: "value",
            // displayField: "name",
        })),

        _actionCmb: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            flex: 1,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new ActionsProxy(tree),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        _actionTxt: new Ext.grid.GridEditor(new Ext.form.TextField({
            flex: 1,
        })),
        get actionId() {
            return isAction ? this._actionTxt : this._actionCmb;
        },

        _store: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new StoresProxy(tree),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        get store() {
            this._store.field.getStore().load();
            return this._store;
        },

        _dataIndex: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new StoreFieldsProxy(tree),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        get dataIndex() {
            this._dataIndex.field.getStore().load();
            return this._dataIndex;
        },

        _idProperty: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new StoreFieldsProxy(tree),
            }),
            triggerAction: "all",
            valueField: "value",
        })),
        get idProperty() {
            this._idProperty.field.getStore().load();
            return this._idProperty;
        },

        _iconCls: new Ext.grid.GridEditor(new Ext.form.ComboBox({
            displayField: "name",
            editable: true,
            forceSelection: false,
            store: new Ext.data.JsonStore({
                fields: ["name", "value"],
                proxy: new CSSRulesProxy(),
            }),
            // tslint:disable-next-line:max-line-length
            tpl: "<tpl for=\".\"><div class=\"x-combo-list-item {value}\" style=\"padding-left: 18px; background-repeat: no-repeat\">{name}</div></tpl>",
            triggerAction: "all",
            valueField: "value",
        })),
        get iconCls() {
            this._iconCls.field.getStore().load();
            return this._iconCls;
        },
    };
}
