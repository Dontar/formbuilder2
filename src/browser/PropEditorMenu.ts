export class PropEditorMenu extends Ext.menu.Menu {
    public aDelProperty: Ext.Action;
    public edPropCombo: Ext.form.ComboBox;
    public menuItemTypes: Ext.menu.Item;

    private currentRecord: Ext.data.Record<any>;

    constructor(private grid: Ext.grid.PropertyGrid) {
        super();
        grid.on("rowcontextmenu", (sender, rowIndex, e: Ext.EventObject) => {
            this.currentRecord = this.grid.getStore().getAt(rowIndex);
            this.aDelProperty.enable();
            this.menuItemTypes.enable();
            this.edPropCombo.reset();
            this.showAt(e.getXY());
            this.edPropCombo.focus.defer(300, this.edPropCombo);
        });
        grid.on("contextmenu", (e: Ext.EventObject) => {
            this.currentRecord = null;
            this.aDelProperty.disable();
            this.menuItemTypes.disable();
            this.edPropCombo.reset();
            this.showAt(e.getXY());
            this.edPropCombo.focus.defer(300, this.edPropCombo);
        });
    }

    public on_edPropCombo_specialkey(sender: Ext.form.ComboBox, e: Ext.EventObject) {
        if (e.getKey() === e.ENTER) {
            this.grid.setProperty(sender.getRawValue(), null, true);
            this.hide();
            this.grid.startEditing.defer(300, this.grid, [this.grid.getStore().getCount() - 1, 1]);
        }
    }

    public on_edPropCombo_select(combo, record, index) {
        this.grid.setProperty(record.data[combo.valueField], null, true);
        this.hide();
        this.grid.startEditing.defer(300, this.grid, [this.grid.getStore().getCount() - 1, 1]);
    }
    public on_aDelProperty_handler() {
        this.grid.removeProperty(this.currentRecord.data.name);
    }

    public initGui() {
        // var props: any = FS.readFileSync(PATH.join(__dirname, "props.json"), "utf8");
        let props: any = require("./tpl/props.json");
        if (props) {
            props = Ext.unique(props);
        } else {
            props = ["no props"];
        }
        Ext.apply(this, {
            items: [{
                text: "Add property",
                xtype: "menutextitem",
            }, {
                ref: "edPropCombo",
                store: props,
                triggerAction: "all",
                xtype: "combo",
            }, "-", {
                iconCls: "icon-tag_blue_edit",
                menu: {
                    items: [
                        { text: "string" },
                        { text: "number" },
                        { text: "boolean" },
                        { text: "date" },
                    ],
                },
                ref: "menuItemTypes",
                text: "Set property type",
                xtype: "menuitem",
            }, "-", this.aDelProperty],
        });
    }

    public onMenuItemTypesItemclick(menuItem: Ext.menu.Item, e: Ext.EventObject) {
        const prop = this.currentRecord.data.name;
        const editor = (this.grid.getColumnModel() as any).editors[(menuItem as any).text];
        (this.grid as any).customEditors[prop] = editor;
    }

    public initComponent() {
        this.initActions();
        this.initGui();
        super.initComponent();
        this.autoConnect();

        this.menuItemTypes.menu.on("itemclick", this.onMenuItemTypesItemclick, this);
    }

    public initActions() {
        const actions = /* actDef */[{
            actionId: "aDelProperty",
            iconCls: "icon-delete",
            text: "Del property",
        }] /* actDef */;

        actions.forEach((action) => {
            this[action.actionId] = new Ext.Action(action);
        });
    }

    public autoConnect() {
        Ext.iterate(Object.getPrototypeOf(this), (key, value, obj) => {
            if (typeof obj[key] === "function") {
                const def = /on_(.+)_(.+)/.exec(key);
                if (def) {
                    if (def[2].toLowerCase() === "handler") {
                        this[def[1]].setHandler(this[key], this);
                    } else if (this[def[1]]) {
                        if (this[def[1]] instanceof Ext.util.Observable && this[def[1]].getSelectionModel) {
                            const sm = this[def[1]].getSelectionModel();
                            this[def[1]].relayEvents(sm, Object.keys(sm.events));
                        }
                        this[def[1]].on(def[2], this[key], this);
                    }
                }
            }
        });
    }
}
