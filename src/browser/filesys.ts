import * as FS from "fs";
import * as PATH from "path";
import { promisify } from "util";

export const writeFile = promisify(FS.writeFile);
export const stat = promisify(FS.stat);
export const rmdir = promisify(FS.rmdir);
export const unlink = promisify(FS.unlink);
export const readFile = promisify(FS.readFile);
export const rename = promisify(FS.rename);
export const mkdir = promisify(FS.mkdir);
// export const readdir = promisify(FS.readdir);

export function readdir(path: string): Promise<Array<{ name: string, path: string, stat: FS.Stats }>> {
    return new Promise((resolve, reject) => {
        FS.readdir(path, (err, files) => {
            err ? reject(err) : resolve(files.map((f) => {
                return {
                    name: f,
                    path: PATH.join(path, f),
                    stat: FS.statSync(PATH.join(path, f)),
                };
            }));
        });
    });
}
